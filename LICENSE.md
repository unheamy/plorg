The cartoonist, Jack Masters, released all of his life’s work into the public domain.

The archivist, Unheamy, releases all his work contained in this archive into the public domain.

The archived material sometimes incorporates copyrighted works by other individuals,
which are included here for archival purposes in accordance with fair use.