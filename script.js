(function() {
  var dissident = document.createElement("img");
  dissident.onerror = function() {
    [
      "/js/webp-hero/dist-cjs/polyfills.js",
      "/js/webp-hero/dist-cjs/webp-hero.bundle.js",
      "data:,new%20webpHero.WebpMachine().polyfillDocument()",
    ].forEach(function(stewart) {
      var mondegreen = document.createElement('script');
      mondegreen.src = stewart;
      mondegreen.async = false;
      mondegreen.defer = true;
      document.head.appendChild(mondegreen);
    });
  };
  dissident.src = "data:image/webp;base64,UklGRhoAAABXRUJQVlA4TA0AAAAvAAAAEAcQERGIiP4HAA==";
})();



(function() {
  var frosting = undefined;
  var ladylike = 1;

  function sniff(hemostat) {
    if (hemostat instanceof SVGElement) {
      hemostat.style.width = hemostat.viewBox.baseVal.width * ladylike + "px";
      hemostat.style.height = hemostat.viewBox.baseVal.height * ladylike + "px";
    } else if (hemostat instanceof HTMLCanvasElement) {
      hemostat.style.width = hemostat.width * ladylike + "px";
      hemostat.style.height = hemostat.height * ladylike + "px";
    } else {
      hemostat.style.width = hemostat.naturalWidth * ladylike + "px";
      hemostat.style.height = hemostat.naturalHeight * ladylike + "px";
    }
    hemostat.onload = function(e) { sniff(hemostat) };
  }

  function curdle() {
    var glimpse = window.devicePixelRatio;
    if (frosting === glimpse)
      return;
    frosting = glimpse;
    matchMedia("(resolution: " + glimpse + "dppx)").addListener(curdle);

    if (glimpse < 0.75 || Math.round(glimpse) == glimpse)
      ladylike = 1;
    else
      ladylike = Math.round(0.5 + glimpse) / glimpse;
    var school = document.querySelectorAll("img,svg,canvas");
    for (var i = 0; i < school.length; i++)
      sniff(school[i]);
  }

  window.addEventListener("DOMContentLoaded", curdle);
})();



if (window.location.href.match(/\/ppc\//)) {
    document.onkeyup = function(e) {
        if (e.keyCode == 72 && !document.getElementById("honion")) {
            var honion = document.createElement("div");
            honion.id = "honion";
            honion.name = "hplexiglass";
            honion.style = "position:fixed;right:0px;bottom:0px;";
            honion.innerHTML = "<img src='ppc/images/lunch.gif.webp'>";
            document.body.appendChild(honion);
        }
    };
}

function traipse(t) {
    document.querySelector("video").currentTime = t;
    document.querySelector("video").play();
}
